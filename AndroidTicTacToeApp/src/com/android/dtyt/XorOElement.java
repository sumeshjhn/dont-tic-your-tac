package com.android.dtyt;

import com.android.dtyt.AndroidTicTacToeAppActivity.State;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.Button;
import android.widget.ImageView;

public class XorOElement extends FrameLayout implements OnClickListener {
	
	boolean test = true;
	
	private final String TAG = "XOOE";
	private Button mBtn;
	private ImageView mXorOImg;
	private AndroidTicTacToeAppActivity mParent;
	private ScanForWinThread mP1;
	
	public XorOElement(Context context, AttributeSet attrs) {
		super(context, attrs);
		mBtn = new Button(this.getContext());
		mXorOImg = new ImageView(this.getContext());	
		initialize();
	}

	private void initialize() {
        mBtn.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.custom_button));
		mBtn.setOnClickListener(this);
		
        this.addView(mBtn);
	}
		

	public void setParent(AndroidTicTacToeAppActivity par)
	{
		this.mParent=par;
		if (mParent.getIsAi())
		{
			int[] tmp = { R.id.element00,
					 R.id.element01,
					 R.id.element02,
					 R.id.element10,
					 R.id.element11,
					 R.id.element12,
					 R.id.element20,
					 R.id.element21,
					 R.id.element22 };
				mParent.setR_ID_xoE(tmp);
		}
	}
	
	public void onClick(View v) {
		int i = 0;
		switch(this.getId())
		{
			case R.id.element00:
			{
				Log.e(TAG, "00");
				i = 0;
				//100 000 000
				//AndroidTicTacToeAppActivity.bitRepOfEl = 0x00000100;
				/*if (test)
				{
					t1.start();
					test = false;
				}
				else
				{
					synchronized (this) {
						this.notify();
					}
				}*/
				break;
			}
			case R.id.element01:
			{
				Log.e(TAG, "01");
				i = 1;
				//010 000 000
				//AndroidTicTacToeAppActivity.bitRepOfEl = 0x00000080;
				break;
			}
			case R.id.element02:
			{
				Log.e(TAG, "02");
				i = 2;
				//001 000 000
				//AndroidTicTacToeAppActivity.bitRepOfEl = 0x00000040;
				break;
			}
			case R.id.element10:
			{
				Log.e(TAG, "10");
				i = 3;
				//000 100 000
				//AndroidTicTacToeAppActivity.bitRepOfEl = 0x00000020;
				break;
			}
			case R.id.element11:
			{
				Log.e(TAG, "11");
				i = 4;
				//000 010 000
				//AndroidTicTacToeAppActivity.bitRepOfEl = 0x00000010;
				break;
			}
			case R.id.element12:
			{
				Log.e(TAG, "12");
				i = 5;
				//000 001 000
				//AndroidTicTacToeAppActivity.bitRepOfEl = 0x00000008;
				break;
			}
			case R.id.element20:
			{
				Log.e(TAG, "20");
				i = 6;
				//000 000 100
				//AndroidTicTacToeAppActivity.bitRepOfEl = 0x00000004;
				break;
			}
			case R.id.element21:
			{
				Log.e(TAG, "21");
				i = 7;
				//000 000 010
				//AndroidTicTacToeAppActivity.bitRepOfEl = 0x00000002;
				break;
			}
			case R.id.element22:
			{
				Log.e(TAG, "22");
				i = 8;
				//000 000 001
				//AndroidTicTacToeAppActivity.bitRepOfEl = 0x00000001;
				break;
			}
		}
		
		//check parent for state
		//player 1 is x for now
		//player 2 is o for now		
    	
		//Log.e(TAG,"t = "+Integer.toBinaryString(AndroidTicTacToeAppActivity.bitRepOfEl[i]));
		//Log.e(TAG,"mParent.getTTTMatrix() = "+Integer.toBinaryString(mParent.getTTTMatrix()));
		//if ( (mParent.getTTTMatrixIndexAt(row,column).compareTo(typeOfOcc.B)==0) && this.getChildCount()<2)
		if ( ((((mParent.getTTTX()|mParent.getTTTO()) & AndroidTicTacToeAppActivity.bitRepOfEl[i])|AndroidTicTacToeAppActivity.bitRepOfEl[i])==AndroidTicTacToeAppActivity.bitRepOfEl[i]) && this.getChildCount()<2)
		{
			if (mParent.getStateOfTTT()==State.PLAYER1)
			{
				this.mXorOImg.setImageDrawable(mParent.getP1PieceDraw());
				//mParent.setTTTMatrixIndexAt(row,column,typeOfOcc.X);
				mParent.setTTTX(mParent.getTTTX()|AndroidTicTacToeAppActivity.bitRepOfEl[i]);
								
			}
			else if (!mParent.getIsAi()&&(mParent.getStateOfTTT()==State.PLAYER2))
			{
				this.mXorOImg.setImageDrawable(mParent.getP2PieceDraw());
				//mParent.setTTTMatrixIndexAt(row,column,typeOfOcc.O);
				mParent.setTTTO(mParent.getTTTO()|AndroidTicTacToeAppActivity.bitRepOfEl[i]);
			}
			
			//mParent.setTTTMatrixIndexAt(row,column,AndroidTicTacToeAppActivity.bitRepOfEl[i]);
			//mParent.setTTTMatrix(AndroidTicTacToeAppActivity.bitRepOfEl[i]);
			this.addView(this.mXorOImg);			
			mP1 = new ScanForWinThread((mParent.getStateOfTTT()==State.PLAYER2)?false:true, this.mParent);
			Thread p1Thread = new Thread(null, mP1);
			p1Thread.start();			
		}
		//disable view
	}

}

package com.android.dtyt;

import java.util.Random;

import com.android.dtyt.AndroidTicTacToeAppActivity.State;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.ImageView;

public class ComputerPlayerThread implements Runnable {

	private AndroidTicTacToeAppActivity mObjSyn;
	private final Random generator = new Random();
	
	public ComputerPlayerThread(AndroidTicTacToeAppActivity objSync)
	{
		this.mObjSyn=objSync;
	}
	
	public void run() {
		
		while (mObjSyn.getStateOfTTT()!=AndroidTicTacToeAppActivity.State.WIN)
		{
			try { Thread.sleep(500); } catch (Exception e) { e.printStackTrace(); }
			synchronized (mObjSyn) {
				if (mObjSyn.getStateOfTTT()!=State.PLAYER2)
				{
					try {
						mObjSyn.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else
				{
					if (mObjSyn.getIsAi()&((mObjSyn.getTTTX()|mObjSyn.getTTTO())!=0x000001FF))
					{
						int j = generator.nextInt(9);
						
						//randomized a lot
						//may have to change the bitRepOfEl (reverse order)
						Log.e("DRAW?", "mObjSyn.getTTTX()="+Integer.toBinaryString(mObjSyn.getTTTX())
								   +"::"+Integer.numberOfLeadingZeros(mObjSyn.getTTTX())
								   +":"+Integer.numberOfTrailingZeros(mObjSyn.getTTTX())
								   +"\nmParent.getTTTO()="+Integer.toBinaryString(mObjSyn.getTTTO())
								   +"::"+Integer.numberOfLeadingZeros(mObjSyn.getTTTO())
								   +":"+Integer.numberOfTrailingZeros(mObjSyn.getTTTO()));
						while (((AndroidTicTacToeAppActivity.bitRepOfEl[j]&(mObjSyn.getTTTX()|mObjSyn.getTTTO()))^AndroidTicTacToeAppActivity.bitRepOfEl[j])==0)
						{
							j = generator.nextInt(9);
							Log.e("random ", "j="+j);
						}
						//Log.e(TAG,"AndroidTicTacToeAppActivity.bitRepOfEl["+j+"] = "+Integer.toBinaryString(AndroidTicTacToeAppActivity.bitRepOfEl[j]));
						//Log.e(TAG, "mObjSyn.getTTTX() = "+Integer.toBinaryString(mObjSyn.getTTTX()));
						//Log.e(TAG, "mObjSyn.getTTTO() = "+Integer.toBinaryString(mObjSyn.getTTTO()));
						
						//the helper should be here...replace this with http://stackoverflow.com/questions/1536654/androidandroid-view-viewrootcalledfromwrongthreadexception-how-to-solve-the
						CPTUpdateUI hRefresh = new CPTUpdateUI(this.mObjSyn,j);
						Log.e("tag", "deeieeiie");
						//hRefresh.sendEmptyMessage(CPTUpdateUI.ADDDIMAGE);
						//Thread dfd = new Thread(null,hRefresh);
						//dfd.start();
						
						//indicate when the loop should end
						//run=false;						
						mObjSyn.runOnUiThread(hRefresh);
						try {
							mObjSyn.wait();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					//mObjSyn.setStateOfTTT(State.PLAYER1);
				}
			}
		}
	}	
}

package com.android.dtyt;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;


public class AndroidTicTacToeAppActivity extends Activity {

	public enum State {
        UNKNOWN,
        WIN,
        PLAYER1,
        PLAYER2 }

	private int[] R_ID_xoE = null;
	
	private Drawable xDr = null;
	private Drawable oDr = null;
	private Drawable p1PieceDraw=null;
	private Drawable p2PieceDraw=null;
	
	private final String TAG = "DTYT";
	private Thread t1;
	/*public static final int[] bitRepOfEl = {	0x00000100,
												0x00000080, 
												0x00000040,
												0x00000020, 
												0x00000010,
												0x00000008, 
												0x00000004, 
												0x00000002,
												0x00000001};*/
	public static final int[] bitRepOfEl = {	0x00000001,
												0x00000002,
												0x00000004,
												0x00000008,
												0x00000010,
												0x00000020,
												0x00000040,
												0x00000080,
												0x00000100};

	private int tttX;
	private int tttO;
	private final int columns = 3;
	private final int rows = 3;
	private int mREQCODE = 2326889;
	private boolean isAi = true;
	private boolean p1Piece = false;
	private Bundle hashmap;
    private State gameState = State.UNKNOWN;
   
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        xDr = this.getResources().getDrawable(R.drawable.x);
        oDr = this.getResources().getDrawable(R.drawable.o);
        tttX = 0x00000000;
        tttO = 0x00000000;
        hashmap = this.getIntent().getExtras();
        if (hashmap!=null)
        {
        	this.isAi = hashmap.getBoolean("enableAI");
        	this.p1Piece = hashmap.getBoolean("XTrueOFalse");
        	this.mREQCODE = hashmap.getInt("reqCode");
        }
        
		this.getIntent().addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        setContentView(R.layout.main);
        initButtons();
        if (this.isAi)
        {
        	if (hashmap!=null)
        	{
        		if (hashmap.getBoolean("removeOldAiThread"))
        		{
        			synchronized(this)
        			{
        				this.notifyAll();
        			}
        		}
        	}
			t1 = new Thread(null, new ComputerPlayerThread(this));
			t1.start();			
        }
        
		if ((this.getP1PieceDraw()==null)&&(this.getP2PieceDraw()==null))
		{
			this.setP2PieceDraw((!(this.getP1Piece()))?this.xDr:this.oDr);
			this.setP1PieceDraw((this.getP1Piece())?this.xDr:this.oDr);
		}
        
   }
    
    private void initButtons()
    {
    	XorOElement btnXOE;    	
    	
    	if (this.gameState==State.UNKNOWN||this.gameState==State.WIN)
    	{
	    	if (hashmap!=null)
	    	{
	    		if (hashmap.containsKey("newBegginingPlayer"))
	    		{
		    		if (hashmap.getBoolean("newBegginingPlayer"))
		    		{
		    			this.gameState=State.PLAYER2;
		    		}
		    		else
		    		{
		    			this.gameState=State.PLAYER1;
		    		}
	    		}
	    		else {
	    			this.gameState=State.PLAYER1;
	    		}
	    	}
	    	else
	    	{
	    		this.gameState=State.PLAYER1;
	    	}
    	}
    	
    	btnXOE = (XorOElement) this.findViewById(R.id.element00);
    	btnXOE.setParent(this);
    	btnXOE = (XorOElement) this.findViewById(R.id.element01);
    	btnXOE.setParent(this);
    	btnXOE = (XorOElement) this.findViewById(R.id.element02);
    	btnXOE.setParent(this);
    	
    	btnXOE = (XorOElement) this.findViewById(R.id.element10);
    	btnXOE.setParent(this);
    	btnXOE = (XorOElement) this.findViewById(R.id.element11);
    	btnXOE.setParent(this);
    	btnXOE = (XorOElement) this.findViewById(R.id.element12);
    	btnXOE.setParent(this);
    	
    	btnXOE = (XorOElement) this.findViewById(R.id.element20);
    	btnXOE.setParent(this);
    	btnXOE = (XorOElement) this.findViewById(R.id.element21);
    	btnXOE.setParent(this);
    	btnXOE = (XorOElement) this.findViewById(R.id.element22);
    	btnXOE.setParent(this);
    	
    	/*buttonMatrix[0][0] = (XorOElement) this.findViewById(R.id.element00);
    	buttonMatrix[0][0].setPlayerOne(mP1);
    	buttonMatrix[0][0].setPlayerTwo(mP2);
    	buttonMatrix[0][0].setParent(this);
    	
    	buttonMatrix[0][1] = (XorOElement) this.findViewById(R.id.element01);
    	buttonMatrix[0][1].setPlayerOne(mP1);
    	buttonMatrix[0][1].setPlayerTwo(mP2);
    	buttonMatrix[0][1].setParent(this);
    	
    	buttonMatrix[0][2] = (XorOElement) this.findViewById(R.id.element02);
    	buttonMatrix[0][2].setPlayerOne(mP1);
    	buttonMatrix[0][2].setPlayerTwo(mP2);
    	buttonMatrix[0][2].setParent(this);
    	
    	buttonMatrix[1][0] = (XorOElement) this.findViewById(R.id.element10);
    	buttonMatrix[1][0].setPlayerOne(mP1);
    	buttonMatrix[1][0].setPlayerTwo(mP2);
    	buttonMatrix[1][0].setParent(this);
    	
    	buttonMatrix[1][1] = (XorOElement) this.findViewById(R.id.element11);
    	buttonMatrix[1][1].setPlayerOne(mP1);
    	buttonMatrix[1][1].setPlayerTwo(mP2);
    	buttonMatrix[1][1].setParent(this);
    	
    	buttonMatrix[1][2] = (XorOElement) this.findViewById(R.id.element12);
    	buttonMatrix[1][2].setPlayerOne(mP1);
    	buttonMatrix[1][2].setPlayerTwo(mP2);  
    	buttonMatrix[1][2].setParent(this);
    	
    	buttonMatrix[2][0] = (XorOElement) this.findViewById(R.id.element20);
    	buttonMatrix[2][0].setPlayerOne(mP1);
    	buttonMatrix[2][0].setPlayerTwo(mP2);
    	buttonMatrix[2][0].setParent(this);
    	
    	buttonMatrix[2][1] = (XorOElement) this.findViewById(R.id.element21);
    	buttonMatrix[2][1].setPlayerOne(mP1);
    	buttonMatrix[2][1].setPlayerTwo(mP2);
    	buttonMatrix[2][1].setParent(this);
    	
    	buttonMatrix[2][2] = (XorOElement) this.findViewById(R.id.element22);
    	buttonMatrix[2][2].setPlayerOne(mP1);
    	buttonMatrix[2][2].setPlayerTwo(mP2);
    	buttonMatrix[2][2].setParent(this);*/
    	
    	/*for (int r = 0; r < rows; r++) {
    		for (int c = 0; c < columns; c++) {
    			tttMatrix[r][c]=typeOfOcc.B;
    		}
    	}*/
		synchronized (this)
		{
				this.notifyAll();
		}
    }
    
    public State getStateOfTTT()
    {
    	return this.gameState;
    }
    
    public void setStateOfTTT(State st)
    {
    	gameState=st;
    }
    
    public int getTTTX()
    {
    	return tttX;
    }
    
    public void setTTTX(int x)
    {
    	tttX = x;
    }
    
    public int getTTTO()
    {
    	return tttO;
    }
    
    public void setTTTO(int o)
    {
    	tttO = o;
    }
        
    public boolean getIsAi()
    {
    	return this.isAi;
    }

    public void setIsAi(boolean ai)
    {
    	this.isAi=ai;
    }
    
    public boolean getP1Piece()//public int getTTTMatrixIndexAt(int r, int c)//public typeOfOcc getTTTMatrixIndexAt (int r, int c)
    {
    	//return tttMatrix[r][c];
    	return p1Piece;
    }
    
    public void setP1Piece(boolean t)//public void setTTTMatrixIndexAt (int r, int c, typeOfOcc t)
    {
    	this.p1Piece=t;
    }

	public int getStartActivityRequestCode()
	{
		return mREQCODE;
	}
	
	public void cleanUpATTTAA()
	{
		this.onStop();
		this.finish();
		this.onDestroy();
	}

	public int[] getR_ID_xoE() {
		return R_ID_xoE;
	}

	public void setR_ID_xoE(int[] r_ID_xoE) {
		this.R_ID_xoE=r_ID_xoE;
	}

	public Drawable getP1PieceDraw() {
		return p1PieceDraw;
	}

	public void setP1PieceDraw(Drawable p1PieceDraw) {
		this.p1PieceDraw = p1PieceDraw;
	}

	public Drawable getP2PieceDraw() {
		return p2PieceDraw;
	}

	public void setP2PieceDraw(Drawable p2PieceDraw) {
		this.p2PieceDraw = p2PieceDraw;
	}
	
	//to disable back button
	/*@Override
	public void onBackPressed() {
	   return;
	}*/
}
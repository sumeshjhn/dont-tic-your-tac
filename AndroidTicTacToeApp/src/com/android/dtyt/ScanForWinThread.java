package com.android.dtyt;

import android.content.Intent;
import android.util.Log;

import com.android.dtyt.AndroidTicTacToeAppActivity.State;


public class ScanForWinThread implements Runnable {
	private final String TAG = "Player";
	private boolean mxTrue_oFalse=true;
	private XorOElement[][] mbtnmtr;
	private int[] winningColumns = {0x00000124, 0x00000092, 0x00000049};
	private int[] winningRows = {0x000001C0, 0x00000038, 0x000007};
	private int winningDiag = 0x00000111;
	private int winningADiag = 0x00000054;
	
	//used to access the button matrix
	private AndroidTicTacToeAppActivity mParent;
	
	public ScanForWinThread(boolean x_o, AndroidTicTacToeAppActivity par)
	{
		this.mxTrue_oFalse = x_o;
		this.mParent = par;
	}
	
	public void run() {
		
		int mat2Chk = (this.mxTrue_oFalse) ? mParent.getTTTX() : mParent.getTTTO();
		boolean diagWin = ((mat2Chk&winningDiag)^winningDiag)==0;
		boolean aDiagWin = ((mat2Chk&winningADiag)^winningADiag)==0;
		boolean colWin=false;
		boolean rowWin=false;
		
		for (int i = 0; i < 3; i++)
		{
			colWin |= ((mat2Chk&winningColumns[i])^winningColumns[i])==0;
			rowWin |= ((mat2Chk&winningRows[i])^winningRows[i])==0;
		}
		
		if (diagWin|aDiagWin|colWin|rowWin)
		{
			boolean b = false;
			if ((mParent.getStateOfTTT()==State.PLAYER2))
			{
				//Log.e(TAG, "PLayer2 wins "+diagWin+" "+aDiagWin+" "+colWin+" "+rowWin);
				//objective is to make opponent win, so player 1 wins if player 2 "wins"
				b =  false;
			}
			else if (mParent.getStateOfTTT()==State.PLAYER1)
			{				
				//Log.e(TAG, "PLayer1 wins "+diagWin+" "+aDiagWin+" "+colWin+" "+rowWin);
				//objective is to make opponent win, so player 2 wins if player 1 "wins"
				b = true;
			}
			mParent.setStateOfTTT(AndroidTicTacToeAppActivity.State.WIN);
			synchronized (mParent)
			{
				mParent.notifyAll();
			}
			Intent retSc = new Intent(mParent, RetryScreen.class);
			retSc.putExtra("legitmate_trueP2FalseP1", b);
			retSc.putExtra("enableAI", mParent.getIsAi());
			retSc.putExtra("XTrueOFalse",mParent.getP1Piece());
			retSc.putExtra("reqCode",mParent.getStartActivityRequestCode());			
			mParent.startActivity(retSc);
			//mParent.cleanUpATTTAA();
		}
		else
		{
			if (mParent.getStateOfTTT()==State.PLAYER2){
				mParent.setStateOfTTT(State.PLAYER1);
			}
			else
			{
				//end of player 1's turn
				mParent.setStateOfTTT(State.PLAYER2);
				if (mParent.getIsAi())
				{
					synchronized (mParent)
					{
						mParent.notifyAll();
					}
				}				
			}
			
			
			Log.e("DRAW?", "mParent.getTTTX()="+Integer.toBinaryString(mParent.getTTTX())
											   +"::"+Integer.numberOfLeadingZeros(mParent.getTTTX())
											   +":"+Integer.numberOfTrailingZeros(mParent.getTTTX())
						+"\nmParent.getTTTO()="+Integer.toBinaryString(mParent.getTTTO())
											   +"::"+Integer.numberOfLeadingZeros(mParent.getTTTO())
											   +":"+Integer.numberOfTrailingZeros(mParent.getTTTO()));
			if (Integer.bitCount(mParent.getTTTX()|mParent.getTTTO())==9)
			{
				Intent retSc = new Intent(mParent, RetryScreen.class);
				retSc.putExtra("cat", true);
				retSc.putExtra("enableAI", mParent.getIsAi());
				retSc.putExtra("XTrueOFalse",mParent.getP1Piece());
				mParent.startActivity(retSc);
			}
		}
		
		
/*		Log.e(TAG, "mat2Chk = "+Integer.toBinaryString(mat2Chk));
		Log.e(TAG, "winningDiag = "+Integer.toBinaryString(winningDiag));
		Log.e(TAG, "winningADiag = "+Integer.toBinaryString(winningADiag));
		for (int i = 0; i < 6; i++)
		{
			if (i < 3)
			{
				Log.e(TAG, "winningColumns["+i+"] = "+Integer.toBinaryString(winningColumns[i]));
			}
			else
			{
				Log.e(TAG, "winningRows["+(i-3)+"] = "+Integer.toBinaryString(winningRows[i-3]));
			}
		}*/
	}

}

package com.android.dtyt;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

public class PickAPiece extends Activity implements OnClickListener {

	private Intent androidTicTacToeAppActivity;
	private ImageView ivP1;
	private ImageView ivP2;
	private ImageView ivSP1;
	private ImageView ivSP2;
	private boolean b = true;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pickapiece);
        
        ivP1 = (ImageView) this.findViewById(R.id.iv_p1);
        ivP2 = (ImageView) this.findViewById(R.id.iv_p2);
        ivSP1 = (ImageView) this.findViewById(R.id.xHigh);
        ivSP2 = (ImageView) this.findViewById(R.id.oHigh);
        ImageButton tmpBtn = (ImageButton) this.findViewById(R.id.btnXPicked);
        tmpBtn.setOnClickListener(this);
        tmpBtn = (ImageButton) this.findViewById(R.id.btnOPicked);
        tmpBtn.setOnClickListener(this);
        tmpBtn = (ImageButton) this.findViewById(R.id.btnNxt);
        tmpBtn.setOnClickListener(this);
    }
	
	public void onClick(View v) {
		switch (v.getId())
		{
		case R.id.btnXPicked:
		{
			this.b = true;
			ivP1.setImageDrawable(this.getResources().getDrawable(R.drawable.playerone));
			ivSP1.setVisibility(ImageView.VISIBLE);
			ivP2.setImageDrawable(this.getResources().getDrawable(R.drawable.playertwo));
			ivSP2.setVisibility(ImageView.INVISIBLE);
			break;
		}
		case R.id.btnOPicked:
		{
			this.b = false;
			ivP2.setImageDrawable(this.getResources().getDrawable(R.drawable.playerone));
			ivSP2.setVisibility(ImageView.VISIBLE);
			ivP1.setImageDrawable(this.getResources().getDrawable(R.drawable.playertwo));
			ivSP1.setVisibility(ImageView.INVISIBLE);
			break;
		}
		case R.id.btnNxt:
		{
			androidTicTacToeAppActivity = new Intent(this, AndroidTicTacToeAppActivity.class);
	        Bundle hashmap = this.getIntent().getExtras();
	        if (hashmap!=null)
	        {
	            androidTicTacToeAppActivity.putExtra("enableAI", hashmap.getBoolean("enableAI"));
	        }
			androidTicTacToeAppActivity.putExtra("XTrueOFalse",this.b);
			androidTicTacToeAppActivity.putExtra("reqCode", 2326889);
			this.startActivityForResult(androidTicTacToeAppActivity,2326889);
			//this.finish();
			
			//chance for memory leak...when going back to pick-a-piece from board.
			
			break;
		}
		}		
	}

}

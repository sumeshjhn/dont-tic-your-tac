package com.android.dtyt;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;

public class SelectModeScreen extends Activity implements OnClickListener {

	private Intent pickAPiece;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selectmodescreen);
        
        ImageButton tmpBtn = (ImageButton) this.findViewById(R.id.btn0);
        tmpBtn.setOnClickListener(this);
        tmpBtn = (ImageButton) this.findViewById(R.id.btn1);
        tmpBtn.setOnClickListener(this);
        
    }
	
	public void onClick(View v) {
		boolean b = false;
		switch (v.getId())
		{
		case R.id.btn0:
		{
			b = true;
			break;
		}
		case R.id.btn1:
		{
			b = false;
			break;
		}
		}
		
		pickAPiece = new Intent(this, PickAPiece.class);

		pickAPiece.putExtra("enableAI",b);
		this.startActivity(pickAPiece);
		//this.finish();
	}

}

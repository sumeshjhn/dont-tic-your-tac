package com.android.dtyt;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;

public class RetryScreen extends Activity implements OnClickListener {

    private boolean b = false;
    private boolean mIsAi = false;
    private boolean mP1Piece = false;
    private int mReqCode = 0;
    private ImageView imgP;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.retryscreen);
        imgP = (ImageView) this.findViewById(R.id.imgP);
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        ImageButton tmpBtn = (ImageButton) this.findViewById(R.id.btnRet);
        tmpBtn.setOnClickListener(this);
        Bundle hashmap = this.getIntent().getExtras();
        boolean catsGame = false;
        if (hashmap!=null)
        {
        	if (hashmap.containsKey("legitmate_trueP2FalseP1"))
        	{
        		this.b = hashmap.getBoolean("legitmate_trueP2FalseP1");
        	}
        	else if (hashmap.containsKey("cat"))
        	{
        		catsGame=hashmap.getBoolean("cat");
        	}
			this.mIsAi=hashmap.getBoolean("enableAI");
			this.mP1Piece=hashmap.getBoolean("XTrueOFalse");
			this.mReqCode=hashmap.getInt("reqCode");
        }
        if (b)
        {
        	imgP.setImageDrawable(this.getResources().getDrawable(R.drawable.p2wins));
        }
        else
        {
        	if (catsGame)
        	{
            	imgP.setImageDrawable(this.getResources().getDrawable(R.drawable.draw));
        	}
        	else
        	{
	        	imgP.setImageDrawable(this.getResources().getDrawable(R.drawable.p1wins));
        	}
        }
    }
	
	public void onClick(View v) {		
		Intent retSc = new Intent(this, AndroidTicTacToeAppActivity.class);
		retSc.putExtra("enableAI",this.mIsAi);
		retSc.putExtra("XTrueOFalse",this.mP1Piece);
		//retSc.putExtra("newBegginingPlayer",(this.mIsAi)?this.b:this.b);
		retSc.putExtra("newBegginingPlayer",this.b);
		retSc.putExtra("reqCode", this.mReqCode);
		retSc.putExtra("removeOldAiThread", true);
		retSc.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);		
		this.startActivity(retSc);
		//this.finish();
	}
	
	//to disable back button
	@Override
	public void onBackPressed() {
	   return;
	}

}
